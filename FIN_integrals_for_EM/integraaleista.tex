\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{fontenc}
\usepackage[finnish]{babel}
\usepackage{amsmath}
\usepackage{parskip}
\usepackage[pdftex]{graphicx}
\usepackage{verbatim}
\usepackage{amssymb}
\usepackage{listings}
\usepackage{float}
\usepackage{epstopdf}

\begin{document}

\begin{center}
\textbf{\Large{Lisätietoa erilaisista integraaleista}}
\end{center}

\vspace{5mm}

Tavallisen integraalin idea on, että käyrän $f(x)$ ja $x$-akselin väliin jäävää pinta-alaa voidaan arvioida summaamalla tutkittavan alueen peittävien palkkien pinta-aloja kuten kuvassa~\ref{integraali}. Yhden $dx$-levyisen palkin ala on $f(x)dx$.

\begin{figure}[H]
\centering
\raisebox{25ex}{$f(x)$}
\includegraphics[width=70mm,trim={10mm 84mm 5mm 75mm},clip]{./figures/integraali.pdf}
\raisebox{0.7ex}{$x$}
\\
\hspace{5mm}
$a$
\hspace{4mm}
$dx$
\hspace{23mm}
$b$
\hspace{3mm}
\caption{Harmaa pinta-ala voidaan laskea summaamalla $dx$ levyisten palkkien aloja. Palkkien korkeus on funktion $f$ arvo kyseisellä kohdalla.}
\label{integraali}
\end{figure}

Mitä ohuempia palkkeja summataan, sitä tarkempi arviosta tulee. Kun palkkeja summataan äärettömästi ja yhden palkin paksuudesta $dx$ tulee infinitesimaalinen, eli juuri ja juuri nollasta poikkeava, niin kyseessä on integrointi

\begin{equation*}
\int_a^b f(x)\textrm{~}dx.
\end{equation*}

\section*{Vektorit integraaleissa}

\subsection*{Polkuintegraali}

Funktioiden lisäksi voidaan integroida myös \textit{vektorifunktioita}

\begin{equation*}
\vec{f} = f_x\hat{x} + f_y\hat{y} + f_z\hat{z},
\end{equation*}

missä $\hat{x}$, $\hat{y}$ ja $\hat{z}$ ovat koordinaattiakselien suuntaiset yksikkövektorit\footnote{Jotkut voivat olla tottuneet käyttämään $\hat{i}$, $\hat{j}$ ja $\hat{k}$ kirjaimia $\hat{x}$, $\hat{y}$ ja $\hat{z}$ sijaan, mikä ajaa täysin saman asian.}. $f_x$, $f_y$ ja $f_z$ voivat olla mitä tahansa funktioita, eivät pelkästään kyseisestä koordinaatista riippuvia. Vektorifunktio voi esimerkiksi kertoa polun kolmiulotteisessa avaruudessa: jokainen polun piste voidaan ilmoittaa $(x,y,z)$ -koordinaateilla ja suunta seuraavaan pisteeseen voidaan antaa vektorina kuten kuvassa~\ref{polku}. Vektorin pituus taas voi välittää lisäinformaatiota, esimerkiksi kappaleen nopeuden kyseisessä polun kohdassa.

\begin{figure}[H]
\centering
\hspace{11mm}
$z$
\hspace{29mm}
$\vec{f}$\\
\includegraphics[width=60mm,trim={10mm 100mm 10mm 20mm},clip]{./figures/polku.pdf}
\raisebox{11ex}{$y$}\\
$x$\hspace{68mm}
\caption{Polku, jonka varrella on kulkusuuntaan osoittavia vektoreita. Katkoviiva on piirretty vain hahmottamisen helpottamiseksi.}
\label{polku}
\end{figure}

Vektorifunktion eri komponentit huomioidaan integroinnissa käyttämällä $dx$:n sijaan infinitesimaalisen lyhyttä vektoria

\begin{equation}
\vec{dl} = dx\hat{x} + dy\hat{y} + dz\hat{z},
\end{equation}

missä $dx$, $dy$ ja $dz$ ovat hyvin pieniä pituuselementtejä, ihan kuin $dx$ aiemmin. Ks. kuva~\ref{dl}.

\begin{figure}[H]
\centering
\hspace{1mm}
$\hat{z}$
\hspace{9mm}
$dy$\\
\raisebox{7ex}{$dz$}
\includegraphics[width=50mm,trim={10mm 110mm 17mm 27mm},clip]{./figures/dl.pdf}
\raisebox{3.5ex}{$dx$}
\hspace{-7mm}
\raisebox{7.5ex}{$y$}
\\
$\hat{x}$
\hspace{27mm}
$\vec{dl}$
\hspace{14mm}
\caption{Infinitesimaalisen vektorin $\vec{dl}$ komponentit.}
\label{dl}
\end{figure}

Vektorimuotoisten asioiden integrointi tapahtuu hyödyntämällä pistetuloa:

\begin{align*}
\int \vec{f} \cdot \vec{dl}
&=
\int \left( f_x dx + f_y dy + f_z dz \right)\\
&=
\int f_x dx + \int f_y dy + \int f_z dz.
\end{align*}

$\vec{f}$:n komponentit integroidaan siis aina niitä vastaavien muuttujien suhteen ja kaikkien integraalien tulokset summataan yhteen. Ideana on tarkastella pieniä polun pätkiä kerrallaan: jos polku on esimerkiksi raketin reitti avaruudessa ja vektorit kertovat moottorin työntövoiman, niin polkuintegraalilla

\begin{equation*}
W = \int \vec{F} \cdot \vec{dl}
\end{equation*}

voitaisiin selvittää matkan aikana tehty työ. Vertaa tätä vaikkapa kaavaan $W=Fx$, polkuintegraalilla vain voidaan lisäksi huomioida voiman suunnan ja suuruuden muutokset matkan aikana.

\subsection*{Pintaintegraali}

\textit{Pintaintegraalissa} käytetään infinitesimaalista \textit{pintaelementtiä}

\begin{equation*}
\vec{dA} = dA\hat{n},
\end{equation*}

missä $dA$ on hyvin pieni palanen tutkittavaa pintaa ja $\hat{n}$ on pinnalle kohtisuora yksikkönormaalivektori kuvassa~\ref{dA} esitetyllä tavalla. Pintaelementti $\vec{dA}$ on vektorimuotoinen siksi, että elementin pinta-alan lisäksi saadaan tietää sen asento $\hat{n}$:n suunnan perusteella. Pintaelementtejä kasaamalla voidaan rakentaa jokin tarkasteltava pinta, esimerkiksi sylinterin vaippa kuten kuvassa~\ref{sylinterinPinta}

\begin{figure}[H]
\centering
$dA$
\hspace{20mm}
$\hat{n}$\\
\includegraphics[width=50mm,trim={0mm 140mm 37mm 55mm},clip]{./figures/pintaelementti.pdf}
\hspace{14mm}
\caption{Pintaelementti sylinteripinnalla.}
\label{dA}
\end{figure}

\begin{figure}[H]
\centering
\includegraphics[width=50mm,trim={0mm 140mm 37mm 70mm},clip]{./figures/daSummaus.pdf}
\hspace{14mm}
\caption{Jos tiedetään sylinterin pintaelementti, niin niitä summaamalla voidaan rakentaa sylinteripinnan mukaista geometriaa. Integrointirajat määrittävät sen, kuinka suuri osa sylinteripinnasta tulee katetuksi.}
\label{sylinterinPinta}
\end{figure}

Pintaintegraalissa

\begin{equation*}
\int \vec{f} \cdot\vec{dA}
=
\int \vec{f} \cdot\hat{n}\textrm{~}dA
\end{equation*}


pistetulo selvittää, miten suuri osa vektorikentästä (esim. pinnan sisällä olevan varauksen aiheuttamasta sähkökentästä) kulkee pinnan läpi tietyssä kohdassa. Tämä selvitetään kaikissa integraalirajojen sisältämissä pisteissä ja summataan yhteen. Huomaa myös, että integroitaessa koko pinnan yli, eli jos integraalirajat kattavat koko pinnan, niin lauseke

\begin{equation*}
\int \hat{n} \cdot\vec{dA}
=
\int \hat{n} \cdot\hat{n}\textrm{~}dA
=
\int \textrm{~}dA
= A
\end{equation*}

tuottaa vain tarkasteltavan kappaleen pinta-alan!

\end{document}
