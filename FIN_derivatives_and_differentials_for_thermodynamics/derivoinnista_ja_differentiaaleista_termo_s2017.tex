\documentclass[12pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{fontenc}
\usepackage[finnish]{babel}
\usepackage{amsmath}
\usepackage{parskip}
\usepackage[pdftex]{graphicx}
\usepackage{verbatim}
\usepackage{amssymb}
\usepackage{mathtools}
\usepackage{listings}
\usepackage{geometry}	%For manipulating page layout
\usepackage{float}
\usepackage[hidelinks]{hyperref}
\usepackage{subfig}	%For including subfigures within figures
\usepackage{epstopdf}

\begin{document}

%TODO: fix referencing

\textbf{\LARGE{Derivoinnista ja differentiaaleista}}\\
{\tiny{T. Mäkelä, \today. Kysymykset ja palaute toni.makela@aalto.fi}}

\section*{Derivaatta}

Lukiosta tuttujen yhden muuttujan funktioiden tapauksessa derivaatta

\begin{equation*}
\frac{df}{dx} = f'
\end{equation*}

antaa funktion $f'(x)$, jonka arvo pisteessä $x = x_0$ on funktion $f(x)$ kulmakerroin samassa pisteessä $x_0$. Tämä seuraa derivaatan määritelmästä erotusosamäärän avulla. Läheisten pisteiden $x_0$ ja $x_1>x_0$ välillä voidaan käyrän kulmakerrointa arvioida kuvan~\ref{derivaatta} tapaan $x$:n ja $f$:n arvojen muutoksien suhteella
\begin{align*}
\frac{\Delta f}{\Delta x}
&= \frac{f(x_1)-f(x_0)}{x_1-x_0}
\textrm{~~~~~~~}|\textrm{~Määritellään~}x_1=x_0+h\\
&= \frac{f(x_0 + h)-f(x_0)}{x_0 + h-x_0}\\
&= \frac{f(x_0 + h)-f(x_0)}{h}.
\end{align*}

\begin{figure}[H]
\centering
\raisebox{21mm}{$\Delta f$}
\hspace{-10mm}
\raisebox{52mm}{$f(x)$}
\hspace{-3mm}
\includegraphics[width=70mm]{./figures/derivaatta.eps}\\
\hspace{12mm}
$\Delta x$
\hspace{42mm}
$x$
\caption{Kun edetään $x$-akselilla välin $\Delta x$ alusta loppuun, funktion $f(x)$ arvo muuttuu $\Delta f$ verran; $\Delta f/\Delta x$ antaa siis punaisen suoran kulmakertoimen. Kun $\Delta x$ puristetaan hyvin pieneksi, niin suorasta tulee käyrän tangentti.}
\label{derivaatta}
\end{figure}

Kuten kuvasta~\ref{derivaatta} voidaan päätellä, niin arvio on sitä tarkempi, mitä pienempi $h$ (eli $\Delta x$) on. Lopulta $h$:n lähestyessä nollaa päästään täsmälliseen arvoon, ja välin $\Delta x$ sijaan voidaan puhua yksittäisestä pisteestä. Välin päätepistehän lähestyy alkupistettä: $\lim_{h\rightarrow 0} x_1 = \lim_{h\rightarrow 0} (x_0 + h) = x_0$. Derivaatta pisteessä $x_0$ määritelläänkin

\begin{equation*}
\frac{df}{dx}(x_0)
=
\lim_{h\rightarrow 0}
\frac{\Delta f}{\Delta x}(x_0)
=
\lim_{h\rightarrow 0}
 \frac{f(x_0 + h)-f(x_0)}{h}.
\end{equation*}

$df$:ssa ja $dx$:ssa "$d$" siis tarkoittaa hyvin pientä muutosta muuttujan $x$ arvossa siinä missä $\Delta x$ yleensä tarkoittaa makroskooppista muutosta. Nyt vain korostetaan sitä, että muutos $dx$ on hyvin pieni -- \textit{infinitesimaalisen pieni}.

\section*{Osittaisderivaatta}

Entä jos funktiolla onkin useampia muuttujia? Otetaan esimerkiksi kahden muuttujan $x$ ja $y$ funktio $f(x,y)$. Tällainen funktio voidaan piirtää kolmiulotteiseen avaruuteen upotettuna kaksiulotteisena pintana kuvan~\ref{partialx} tapaan. Jos tämä pinta leikataan äärettömän moneksi $x$ akselin suuntaiseksi suikaleeksi, niin jokainen differentiaalisen ohut suikale on käyrä. Nämä käyrät voidaan taas palauttaa yhden muuttujan funktioksi, jossa toinen muuttuja $y$ on vain korvattu vakiolla: sehän saa saman arvon käyrän joka pisteessä.

\begin{figure}[H]
\centering
\hspace{14mm}
$f(x,y)$
\hspace{3mm}
$f(x,y=a)$
\hspace{3mm}
$f(x,y=b)$\\
\includegraphics[width=70mm,trim={5mm 102mm 5mm 50mm},clip]{./figures/partialx.pdf}
\raisebox{21mm}{$y$}\\
\hspace{-35mm}
$x$
\hspace{7mm}
$y=a$
\hspace{7mm}
$y=b$
\caption{$f(x,y=a)$ ja $f(x,y=b)$ ovat esimerkkejä käyristä, joilla $y$ on vakio.}
\label{partialx}
\end{figure}

Vastaavasti voidaan toki leikata pinta $y$-akselin suuntaisesti kuvan~\ref{partialy} tapaan. Tällöin saadaan käyriä, joilla $x$ on vakio.

\begin{figure}[H]
\centering
\hspace{-20mm}
$f(x,y)$\\
\raisebox{18mm}{
$f(x=d,y)$
}
\hspace{-26mm}
\raisebox{39mm}{
$f(x=c,y)$
}
\hspace{-2mm}
\includegraphics[width=70mm,trim={14mm 102mm 5mm 50mm},clip]{./figures/partialy.pdf}
\raisebox{9mm}{$x=d$}
\hspace{-13mm}
\raisebox{16mm}{$x=c$}
\hspace{-12mm}
\raisebox{22mm}{$y$}\\
\hspace{-60mm}
$x$
\caption{$f(x=c,y)$ ja $f(x=d,y)$ ovat esimerkkejä käyristä, joilla $x$ on vakio.}
\label{partialy}
\end{figure}

Yhden muuttujan funktiolle derivaatta siis antoi käyrän kulmakertoimet. Mitä siis tarkoittaa kahden muuttujan funktion derivaatta ja miten sellainen edes otetaan? Määritellään seuraavaksi vähän työkaluja tähän tarkoitukseen. Funktion $f(x,y)$ \textit{osittaisderivaatta} $x$:n suhteen tarkoittaa sitä, että asetetaan $y$:lle jokin arvo, esimerkiksi $a$, ja pidetään se vakiona. Tällöin osittaisderivaatta, jota voidaan merkitä esim. seuraavilla tavoilla

\begin{equation*}
\left(
\frac{\partial f}{\partial x}
\right)_y
=
(\partial f/\partial x)_y
=
(\partial_x f)_y
\end{equation*}

antaa käyrän $f(x,y=a)$ kulmakertoimet. Notaatio on uusi, mutta "$\partial$" (lausutaan "doo") ei tarkoita mitään ihmeellistä: derivaatan ottaminen tapahtuu ihan samoin kuin yhden muuttujan funktiolla. "$\partial$":n käyttäminen "$d$":n sijaan vain muistuttaa meitä siitä, että kyseessä on alunperin ollut useamman muuttujan funktio. Derivaattaa otettaessa vakioiksi asetetut muuttujat merkitään yleensä alaindekseinä derivaattalauseketta ympäröivien sulkujen jälkeen, kuten "$(~)_y$" yllä. Samaan tapaan voidaan ottaa osittaisderivaatta $y$:n suhteen pitäen $x$ vakiona: $(\partial f / \partial y)_x$.

\textbf{Esimerkki:} $f(x,y) = x^4 + x^2 y + cy^5 + d$, missä $c$ ja $d$ ovat vakioita. Tällöin

\begin{equation*}
\left(
\frac{\partial f}{\partial x}
\right)_y
=
4x^3 + 2x y
\textrm{~~~~~ja~~~~}
\left(
\frac{\partial f}{\partial y}
\right)_x
=
x^2 + 5cy^4.
\end{equation*}

\subsection*{Ketjusääntö}

Funktiolla on joskus myös \textit{sisäfunktio}, esimerkiksi $f(g(x))$. Tällöin funktion $f$ derivaatta $x$:n suhteen otetaan seuraavasti:

\begin{equation*}
\frac{d f}{d x} =
\left(\frac{d f}{d g}\right) \left(\frac{d g}{d x}\right)
\end{equation*}

Osittaisderivaattaan $\partial/\partial x$ liittyy oikeastaan vain yksi seikka, joka erottaa sen "tavallisesta" kokonaisderivaatasta $d/dx$. Pureudutaan nyt siihen. Useamman muuttujan funktion tapauksessa $x$ ja $y$ voivat ihan hyvin olla funktioita: jos ne edustaisivat vaikkapa kappaleen koordinaatteja kaksiulotteisella kartalla, niin niiden arvo voi riippua ajasta, mikäli kappale liikkuu. Funktion $f(x(t),y(t))$ derivaatta $t$:n suhteen saadaan niin kutsutusta \textit{ketjusäännöstä}

\begin{equation}
\frac{df}{dt}
=
\left(
\frac{\partial f}{\partial x}
\right)_y
\frac{dx}{dt}
+
\left(
\frac{\partial f}{\partial y}
\right)_x
\frac{dy}{dt}
\label{ketjus}
\end{equation}

\textbf{Esimerkki:} $f(x,y) = x^2 + x y^2 + y$ ja $x(t)=t^2+t$, $y(t)=3t$. Tällöin yhtälön~\eqref{ketjus} mukaan

\begin{align*}
\frac{df}{dt}
&=
\left(
\frac{\partial f}{\partial x}
\right)_y
\frac{dx}{dt}
+
\left(
\frac{\partial f}{\partial y}
\right)_x
\frac{dy}{dt}\\
%&=
%\left(
%\frac{\partial}{\partial x}
%(x^2 + x y^2 + y)
%\right)_y
%\frac{dx}{dt}
%+
%\left(
%\frac{\partial}{\partial y}
%(x^2 + x y^2 + y)
%\right)_x
%\frac{dy}{dt}\\
&=
(2x + y^2)
\frac{dx}{dt}
+
(2xy + 1)
\frac{dy}{dt}
\textrm{~~~~~}|\textrm{~Sijoitetaan~}x,y\\
&=
(2(t^2+t) + (3t)^2)
\frac{d}{dt}(t^2+t)
+
(2(t^2+t)(3t) + 1)
\frac{d}{dt}3t\\
&=
(2t^2+2t + 9t^2)
(2t+1)
+
(6t^3+6t^2 + 1)3\\
%&=
%4t^3+4t^2 + 18t^3
%+2t^2+2t + 9t^2
%+
%18t^3+18t^2 + 3\\
&=
40t^3 +33t^2 +2t +3
\end{align*}

Näin saatu tulos on täsmälleen sama kuin jos kirjoitettaisiin
\begin{align*}
f(x(t),y(t)) &= x^2 + x y^2 + y
\textrm{~~~~~}|\textrm{~Sijoitetaan~}x,y\\
&= (t^2+t)^2 + (t^2+t) (3t)^2 + 3t\\
&= 10t^4 + 11t^3 + t^2 + 3t
\end{align*}
ja laskettaisiin suoraan näin saadun lausekkeen derivaatta $t$:n suhteen. Tässä funktio ja sisäfunktiot olivat melko yksinkertaisia, joten lasku on molemmilla tavoilla tehtynä lyhyt. Useimmiten sisäfunktiot ovat kuitenkin monimutkaisia, ja ketjusään-nön käyttäminen sattaa yksinkertaistaa laskua huomattavasti.

Funktiolla $f$ voi kuitenkin olla myös sellaista riippuvuutta muuttujasta $t$, joka ei liity $x$:n ja $y$:n kulloisiinkin arvoihin. Funktio voisi olla vaikkapa vaellusretkellä mitattu lämpötila: jos vaeltaja kapuaa korkealle vuorelle, lämpötila laskee. Mitattu arvo siis vaihtelee paikan funktiona. Toisaalta päivälämpötila vaihtelee tavallisesti ajan funktiona vaikka vaeltaja pysyisi paikallaankin. Jos siis halutaan funktion $f(x(t),y(t),t)$ \textit{kokonais}derivaatta $t$:n suhteen, eli $df/dt$, niin se saadaan lausekkeesta

\begin{equation}
\frac{df}{dt}
=
\left(
\frac{\partial f}{\partial x}
\right)_y
\frac{dx}{dt}
+
\left(
\frac{\partial f}{\partial y}
\right)_x
\frac{dy}{dt}
+
\left(
\frac{\partial f}{\partial t}
\right)_{x,y},
\label{total}
\end{equation}

missä yhtälön oikealla puolella esiintyvä \textit{osittais}derivaatta $(\partial f/\partial t)_{x,y}$ koskee vain niitä $t$-muuttujia, jotka ovat kyseistä termiä laskiessa vakioiksi asetettujen $x$:n ja $y$:n ulkopuolella. Miksi? Jos vaikkapa funktio $f(x(t),y(t),t) = x^2 + x y^2 + y + t$ kirjoitettaisiin auki pelkästään $t$:n funktioksi (kuten ylemmän esimerkin vaihto-ehtoisessa tavassa) ja otettaisiin derivaatta $t$:n suhteen, niin tulos ei olisi sama kuin yhtälöllä~\eqref{ketjus}, joka ei käsittelisi $f$:n viimeistä termiä ollenkaan! Oikea tulos saadaan yhtälöstä~\eqref{total}, joka huomioi tarvittavan lisäyksen. Jos siis kuulet väitettävän, että osittaisderivaatta ei ole sama asia kuin "tavallinen" derivaatta, niin kyse on nimenomaan yhtälön~\eqref{total} mukaisesta erosta kokonaisderivaatan $df/dt$ ja sen osan $(\partial f / \partial t)_{x,y}$ välillä.

\subsection*{Differentiaaleista ja integroinnista}

Jos funktion $f(x,y)$ arvo muuttuu $df$ verran, niin muutoksen täytyy liittyä jotenkin $x$:n ja $y$:n muutoksiin $dx$ ja $dy$. Tämä muutos voidaan ilmoittaa \textit{differentiaalina}, joka on yleisesti ottaen muotoa $df = g(x,y)dx + h(x,y)dy$. Esimerkiksi se, kuinka paljon $x$:n muuttuminen muuttaa funktion arvoa, voi riippua kulloinkin tutkittavasta paikasta. Tästä syystä $dx$:n edessä on jokin $x$:n ja $y$:n funktio. Sama pätee $dy$-termiin. Samaan tapaan kuin $\int_{x_1}^{x_2} dx = x_2 - x_1 = \Delta x $, voidaan jokin makroskooppinen funktion $f$ arvon muutos laskea integraalista $\Delta f = \int df$. On kuitenkin huomattava, että useamman muuttujan funktion integroinnissa on valittava \textit{reitti}, jota pitkin integroida. Tätä on havainnollistettu kuvassa~\ref{integraalireitit}.

\begin{figure}[H]
\centering
\hspace{15mm}
$x$
\hspace{15mm}
\raisebox{2mm}{$y_1$}
\hspace{2mm}
\raisebox{55mm}{$f(x,y)$}
\hspace{13mm}
\raisebox{2mm}{$y_2$}
\hspace{1mm}
\raisebox{4mm}{$x_2$}
\hspace{14mm}
\raisebox{19mm}{$x_1$}
\hspace{-88mm}
\includegraphics[width=90mm]{./figures/integraalireitit.eps}
\raisebox{23mm}{$y$}
\caption{Integroitaessa funktiota $f(x,y)$ välillä $(x_1,y_1)\rightarrow(x_2,y_2)$ lasketaan $xy$-tason ja  funktion määrittämällä pinnalla kulkevan reitin väliin jäävä pinta-ala. Jos reitiksi valitaan $(x_1,y_1)\rightarrow(x_1,y_2)\rightarrow(x_2,y_2)$, niin lasketaan sinisellä rajattu pinta-ala. Reitillä $(x_1,y_1)\rightarrow(x_2,y_1)\rightarrow(x_2,y_2)$ puolestaan lasketaan punaisella rajattu ala.}
\label{integraalireitit}
\end{figure}

Miten integrointi sitten käytännössä tapahtuu? Jos halutaan esimerkiksi integroida $f(x,y)$ pisteestä $(x_1,y_1)$ pisteeseen $(x_2,y_2)$ reittiä $(x_1,y_1)\rightarrow(x_1,y_2)\rightarrow(x_2,y_2)$ pitkin, niin kokonaismuutos voidaan hajottaa muotoon
\begin{equation*}
\Delta f_{(x_1 \rightarrow x_2, y_1\rightarrow y_2)}
 = \Delta f_{(x_1, y_1\rightarrow y_2)}
 + \Delta f_{(x_1\rightarrow x_2, y_2)},
\end{equation*}
mistä esimerkiksi ensimmäinen palanen laskettaisiin seuraavasti:
\begin{equation*}
\Delta f_{(x_1, y_1\rightarrow y_2)}
 = \int_{x_1,y_1}^{x_1,y_2} df
 = \int_{x_1}^{x^1} g(x_1,y)dx + \int_{y_1}^{y^2} h(x_1,y)dy
 = \int_{y_1}^{y^2} h(x_1,y)dy.
\end{equation*}

Huomaa, että tätä osaa integroitaessa $x=x_1$. Vastaavasti osassa $\Delta f_{(x_1\rightarrow x_2, y_2)}$ asetetaan $y=y_2$. Jos jotain differentiaalia integroitaessa saadaan sama arvo riippumatta valitusta reitistä, niin differentiaali on \textit{eksakti}. Eksakti differentiaali $df$ on myös sellaista muotoa, että voidaan määrittää jokin funktio $f(x,y)$, jolle pätee
\begin{equation}
df = \left(\frac{\partial f}{\partial x}\right)_y dx
   + \left(\frac{\partial f}{\partial y}\right)_x dy.
\label{kokonaisdifferentiaali}
\end{equation}

Lauseketta~\eqref{kokonaisdifferentiaali} sanotaan funktion $f(x,y)$ \textit{kokonaisdifferentiaaliksi}.

Vaikka tässä materiaalissa rajoituttiinkin yhden ja kahden muuttujan funktioihin, niin periaatteet voidaan yleistää useammillekin muuttujille. Mieti esimerkiksi, mikä on lausekkeiden~\eqref{total} ja~\eqref{kokonaisdifferentiaali} logiikka ja miten voisit lisätä niihin uusia muuttujia sitä seuraamalla.

\end{document}
