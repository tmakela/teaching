\documentclass{article}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage{fontenc}
\usepackage{amsmath}
\usepackage{parskip}
\usepackage[pdftex]{graphicx}
\usepackage{verbatim}
\usepackage{amssymb}
\usepackage{bm}
\usepackage{mathtools}
\usepackage{mathrsfs}
\usepackage{listings}
\usepackage{geometry}	%For manipulating page layout
\usepackage{float}	%Better control of figure placement
\usepackage[hidelinks]{hyperref}
\usepackage[titletoc]{appendix} %For including appendices if need be
\usepackage{lastpage}	%For counting pagenumbers
\usepackage{subfig}	%For including subfigures within figures
\usepackage{anyfontsize}%For changing font sizes mid-document
\usepackage[backend=biber,	%Use biber engine
	    natbib=true,
	    style=numeric,	%View references as numbers
	    sorting=none,
	    hyperref=true
]{biblatex}
\usepackage{epstopdf}
\usepackage{subfig}	%For including subfigures within figures
\usepackage{changepage}
\usepackage{tikz-cd}

% Import the bibtex database
\addbibresource{references.bib}

\numberwithin{equation}{section}	%Number equations by sections

%Abbreviations
\newcommand{\R}{\mathbb{R}}  %Real numbers
\newcommand{\C}{\mathbb{C}}  %Complex numbers
\newcommand{\K}{\mathbb{K}}  %Scalar field
\newcommand{\Z}{\mathbb{Z}}  %Integers
\newcommand{\N}{\mathbb{N}}  %Natural numbers

%Declare operators in the style of e.g. sin() 
\DeclareMathOperator{\diag}{diag} 	%A diagonal matrix

\begin{document}

\textbf{\huge Lecture 1}

\textbf{\large Learning goals}
\begin{itemize}
\item Bra and ket spaces.
\item Postulates of QM.
\item Completeness relation.
\item Global phase is arbitrary, relative phases matter.
\item Generating translations in space and time.
\end{itemize}

\section{States in Hilbert space} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

A \textit{Hilbert space} is defined as a complete inner product space. A \textit{state} is some combination of values in a collection observables (=measurable quantities) describing the system. As a classical analog, in thermodynamics the equations of state describe a system in terms of volume, pressure and temperature. In quantum mechanics we are most commonly interested in Hilbert spaces whose elements are either \textit{complex vectors} or \textit{square integrable complex functions}. Notice that the space of the linear functions acting on a vector and producing a number is a vector space by itself. Any such function can indeed be written -- in exactly one way -- as something we call a \textit{dual vector}. The essential difference between vectors and dual vectors is merely that they live in different vector spaces. We can however define an operation that takes an element from both a vector space and its dual and returns a number: \textit{the inner product}, which generalizes the notion of the dot or scalar product.

As an example, consider the column and row vectors of linear algebra. If we take columns to be the "usual" vectors, then rows are their "duals": a row acting on a column from the left produces a number. For $n$-dimensional complex row and column vectors, we have
\begin{equation*}
\begin{bmatrix}
a_1 &a_2 &... &a_n
\end{bmatrix}
\begin{bmatrix}
b_1\\
b_2\\
\vdots\\
b_n
\end{bmatrix}
=
c
\in \C.
\end{equation*}
In quantum mechanics, the kets are usually chosen to be the "regular" vectors and bras the duals, so that a bra $\langle \beta|$ acting on a ket $|\alpha\rangle$ produces a number
\begin{equation*}
\langle\beta|\alpha\rangle \in \C.
\end{equation*}
Whenever some vector space is the dual of some other, we must have a one-to-one correspondence between the elements of the two spaces. There must be some way to turn a vector, say $\bm{v}$, into its dual, say $\bm{v}'$, so that when we again take the dual of $\bm{v}'$ we get back to the $\bm{v}$ we started with. For real row and column vectors, this is done by the transpose. For bra and ket spaces, the analogous operation is the Hermitian conjugation
\begin{equation*}
|\psi\rangle^\dagger = \langle\psi|
\quad\textrm{ and }\quad
|\psi\rangle = \langle\psi|^\dagger.
\end{equation*}
For finite-dimensional Hilbert spaces, we may write bras in row vector form and kets as columns. In this case Hermitian conjugation means taking the transpose and turning all elements into their complex conjugates:
\begin{equation}
|\psi\rangle=
\begin{bmatrix}
c_1\\
c_2\\
\vdots\\
c_n
\end{bmatrix}
\leftrightarrow
\langle \psi|
=
\begin{bmatrix}
c_1^* &c_2^* &... &c_n^*
\end{bmatrix},
\quad c_i \in \C \quad \forall~~i\in\{1,2..,n\}.
\label{ketColBraRow}
\end{equation}
Here "$\leftrightarrow$" denotes the dual correspondence between the two objects. A Hilbert space spanned by an orthonormal basis lets us write its elements in a form that gives rise to \textit{superposition}. If a ket space is spanned by the orthonormal set $\{|n\rangle\}_{n=1}^N$ (so that $\langle m|n \rangle = \delta_{mn}$), we write for a ket $|\psi\rangle$ and its dual
\begin{align}
\begin{cases}
|\psi\rangle
=
\sum_{n=1}^N c_n |n\rangle &,
  \quad c_n = \langle n|\psi\rangle.
\\
\langle\psi|
=
\sum_{m=1}^N c_m^* \langle m| &,
  \quad c_m^* = \langle m|\psi\rangle^\dagger
              = \langle \psi|m\rangle.
\end{cases}
\label{ketBraSeries}
\end{align}

There are various ways to map an element of a vector space into another. For instance, a spatial position vector can be transformed by rotation matrices. In quantum mechanics, such things are done by \textit{operators}. Whereas bras produce numbers out of kets, operators act on a ket to produce another ket -- or on a bra to produce another bra. In finite-dimensional Hilbert spaces, operators can be written in matrix form. On the other hand, if the Hilbert spaces is home to continuous functions, a common example of an operator is differentiation.

\section{The postulates of quantum mechanics} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

In quantum mechanics, postulates hold a position very similar to that of axioms in mathematics. The below statements are the cornerstones that non-relativistic quantum theory is built on.

\begin{tabular}{c l}
I   & At each moment, the \textbf{state} of a quantum system is represented by 
      a vector in a Hilbert\\~ & space.\\
II  & \textbf{Observables} are described by linear, Hermitian operators.\\
III & A \textbf{measurement} of the observable $A$ (some physical quantity of 
      the system) must result\\~& in some eigenvalue of the operator $\hat{A}$. 
      The eigenvalue $a_n$ with an eigenstate $|a_n\rangle$ occurs\\~& 
      with the probability $P(a_n)=|\langle a_n |\psi\rangle|^2$. 
      Immediately after the measurement, the\\~& state of the system is 
      $|a_n\rangle$.\\
IV  & The \textbf{time-evolution} of a system that is not subject to 
      measurement is described by the\\~&\textit{Schrödinger equation} 
      $\hat{H}|\psi\rangle = ih\frac{d}{dt}|\psi\rangle$.\\
\end{tabular}

Quantum mechanics has been developed as a way to explain \textit{observations} that were out of the scope of classical mechanics. As a result, physicists don't quite agree on the interpretation of various matters in the theory. As an example, the \textbf{collapse of the wavefunction} is historically a somewhat controversial topic, but has strong experimental support. This is built into the formulation of postulate III: if a measurement yields some value, a subsequent measurement must have the same result.

\section{Completeness} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Mathematically speaking, a \textit{complete} space is one in which all Cauchy sequences converge to an element that belongs to the same space. When a physicist refers to completeness, it usually suffices to be aware of the \textbf{completeness relation}: let $|n\rangle$, with $n\in\N$, be an orthonormal basis of a finite-dimensional Hilbert space. Then the \textit{identity operator} can be written as
\begin{equation}
\sum_n |n\rangle\langle n| = \hat{I}.
\label{completenessDiscrete}
\end{equation}
A Hilbert space may also well be infinite-dimensional. As perhaps the most common example, the position $x$ is widespreadly assumed to be a continuous variable. Then if we were to measure the position of a particle, and each resulting eigenvalue corresponds to a distinct eigenstate, the Hilbert space must be spanned by an infinite number of position eigenstates! For such spaces, the summation in Eq.~\eqref{completenessDiscrete} is replaced by an integral and we write the completeness relation as
\begin{equation}
\int_{-\infty}^\infty |x\rangle\langle x|dx = \hat{I}.
\label{completenessContinuous}
\end{equation}
The importance of the completeness relations in Eq.s~\eqref{completenessDiscrete} and~\eqref{completenessContinuous} can hardly be exaggerated, as one may -- and quite often will -- insert a well-chosen identity pretty much anywhere in an equation.

\subsection{The inner product and connection to wavefunctions} %%%%%%%%%%%%%%%%

In the \textbf{discrete} case, the inner product of two states can be written as
\begin{equation*}
\langle \alpha | \beta \rangle
=
\left(\sum_n \langle n|a_n^* \right) \left(\sum_m b_m|m \rangle\right)
=
\begin{bmatrix}
a_1^* &a_2^* &... &a_n^*
\end{bmatrix}
\begin{bmatrix}
b_1\\
b_2\\
\vdots\\
b_n
\end{bmatrix}.
\end{equation*}
The last equality holds only for finite-dimensional Hilbert spaces -- for countably infinite-dimensional spaces, we must write things in the form of the first equality.

When considering uncountably infinite-dimensional Hilbert spaces, we will in practice be working with \textbf{continuous} functions. For a continuous variable, such as position, we define the \textit{wavefunction} as
\begin{equation*}
\psi(x) \equiv \langle x|\psi\rangle.
\end{equation*}
It has the complex conjugate
\begin{equation*}
\psi^*(x)
\equiv (\psi(x))^*
     = (\langle x|\psi\rangle)^\dagger
     = \langle\psi|x\rangle.
\end{equation*}
Invoking the completeness relation of~\eqref{completenessContinuous} now reveals the inner product to become that of continuous square-integrable functions:
\begin{equation*}
\langle \phi|\psi\rangle
=
\int_{-\infty}^\infty \langle \phi|x\rangle\langle x| \psi\rangle dx
=
\int_{-\infty}^\infty (\langle x|\phi\rangle)^\dagger\langle x| \psi\rangle dx
=
\int_{-\infty}^\infty \phi^*(x)\psi(x) dx.
\end{equation*}

The usual prescription for normalizing states is to demand $|\psi|^2=1$. The \textbf{properties of the inner product} are
\begin{itemize}
\item linearity:
$\langle \phi|\left(a|\alpha\rangle + b|\beta\rangle\right)
=
a\langle \phi|\alpha\rangle + b\langle \phi|\beta\rangle$.

\item skew symmetry: $\langle \psi|\phi\rangle = \langle \phi|\psi\rangle^*$.

\item positive definiteness: $\langle \psi|\psi\rangle \equiv |\psi|^2 \geq 0$ ($0$ iff $|\psi\rangle = 0$).
\end{itemize}

\subsection{Observables as linear Hermitian operators} %%%%%%%%%%%%%%%%%%%%%%%%

An operator $\hat{A}$ is \textit{linear}, if it holds that
\begin{equation*}
\hat{A}(a|\alpha\rangle + b|\beta\rangle)
=
a\hat{A}|\alpha\rangle + b\hat{A}|\beta\rangle.
\end{equation*}
It is \textit{Hermitian} (=self-adjoint), if
\begin{equation*}
\langle \psi | \hat{A} \phi \rangle
=
\langle \psi \hat{A} | \phi \rangle
\equiv
\langle \psi | \hat{A} | \phi \rangle,
\end{equation*}
which is to say that $\hat{A}=\hat{A}^\dagger$. The \textbf{Spectral theorem} states that \textit{Hermitian operators can always be diagonalized and that their eigenvalues are real numbers}. This makes them good for describing physical quantities, since experimental results are always real-valued: the devices used in laboratories give their results as some number such as voltage. It is also a very handy property of Hermitian operators that the eigenvectors with distinct eigenvalues are orthogonal.

Credible measurements need statistics. In quantum physics it is often things like expectation values and standard deviations that can be predicted and then tested in a lab -- not the outcome of a single experiment. With the usual normalization $|\psi|^2=1$, the expectation value of an operator is
\begin{equation*}
\langle \hat{A} \rangle
= \langle \psi| \hat{A} |\psi \rangle
= \langle \psi| \hat{A} \sum_n |n\rangle\langle n|\psi \rangle
= \sum_n \langle \psi| a_n |n\rangle\langle n|\psi \rangle
= \sum_n a_n |\langle n | \psi \rangle|^2
\equiv \sum_n a_n P_n,
\end{equation*}
where we have defined the probability $P_n$ for the system to collapse into the state $|n\rangle$ at the time of measurement. The expecation value tells us the average result if we'd perform the same measurement on a large number of systems that have been prepared \textit{identically}. If the state is not normalized, the normalization must be included in the definition of probability so that we again get probabilities within $[0,1]$ and that sum to 1 if we take all possible outcomes into account. The generalized propability is
\begin{equation}
P_n = \frac{|\langle n|\psi\rangle|^2}
           {\langle\psi|\psi\rangle}
\label{genProb}
\end{equation}
and the generalized expectation value is
\begin{equation*}
\langle\hat{A}\rangle = \frac{\langle\psi|\hat{A}|\psi\rangle}
                             {\langle\psi|\psi\rangle}.
\end{equation*}

The \textit{variance} of a set of measurements is given by
\begin{equation*}
\sigma_{\hat{A}}^2 = \langle(\hat{A} - \langle\hat{A}\rangle)^2\rangle.
\end{equation*}
This is the square of the standard deviation $\sigma_{\hat{A}}$. A useful alternative form for the variance is $\sigma_{\hat{A}}^2 = \langle \hat{A}^2 \rangle -  \langle \hat{A} \rangle^2$. In case of doubt, check!

\subsubsection{Operations in combined Hilbert spaces}

The state of one quantum mechanical system is described by a vector in one Hilbert space. Now let us combine \textit{two} systems $S_1$ and $S_2$, described by the Hilbert spaces $\mathcal{H}_1$ (spanned by $\{|n\rangle_1\}$) and $\mathcal{H}_2$ (spanned by $\{|m\rangle_2\}$). The Hilbert space $\mathcal{H}_1\times\mathcal{H}_2$ describing the combination is spanned by \textit{product states} of the form $|n\rangle_1\otimes|m\rangle_2 \equiv |n\rangle|m\rangle \equiv |n,m\rangle$. To operate on such states, we need to know how the operation should affect the parts 1 and 2. Generally we do
\begin{equation*}
\hat{A}_1\otimes\hat{A}_2 |n\rangle_1\otimes|n\rangle_2
=
(\hat{A}_1|n\rangle_1)\otimes(\hat{A}_2|n\rangle_2).
\end{equation*}
Short-hand operations such as $\hat{A}_2 |n\rangle_1\otimes|n\rangle_2$ usually stand for $\hat{I}_1\otimes\hat{A}_2 |n\rangle_1\otimes|n\rangle_2$. 

\section{Phase-shifts} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Let us examine the two states $|\psi\rangle$ and $|\psi'\rangle = e^{i\theta}|\psi\rangle$, $\theta \in \R$. It holds that
\begin{align*}
\begin{cases}
\textrm{if one is normalized, so is the other one: } 
&
\langle\psi|\psi\rangle
= \langle\psi|e^{-i\theta}e^{i\theta}|\psi\rangle
= \langle\psi'|\psi'\rangle
\\
\textrm{both have the same probabilities: } 
&
|\langle n|\psi\rangle|^2
= |e^{i\theta}\langle n|\psi\rangle|^2
= |\langle n|\psi'\rangle|^2
\end{cases}.
\end{align*}

In principle, we could examine the state $|\tilde{\psi}\rangle = \alpha e^{i\theta}|\psi\rangle$, $\alpha\in\C$. Notice that the generalized probability of Eq.~\eqref{genProb} gives the same $P_n$ for both the state $|\psi\rangle$ and $|\tilde{\psi}\rangle$. This means that the \textit{physical} states are \textit{rays in a Hilbert space}, ergo non-zero vectors modulo multiplication by a non-zero complex scalar.

If $|\psi_1\rangle$ and $|\psi_2\rangle$ belong to the same Hilbert space, then $|\psi_1\rangle + |\psi_2\rangle \equiv |\psi\rangle$ belongs to that space, too. By now we know that we can write $|\tilde{\psi}_n\rangle = \alpha_n e^{i\theta_n}|\psi_n\rangle$ and get the same measurement outcomes for $|\tilde{\psi}_n\rangle$ and $|\psi_n\rangle$. However, in general
\begin{equation*}
|\phi\rangle
\equiv e^{i\theta_1}|\psi_1\rangle
     + e^{i\theta_2}|\psi_2\rangle
\neq
|\psi\rangle
\equiv |\psi_1\rangle
     + |\psi_2\rangle.
\end{equation*}
We have $|\psi\rangle = |\phi\rangle$ only if there is a relation like $\theta_1 = \theta_2+2n\pi$, $n\in\Z$ between the two phases. In this case $\theta_2$ (say) is a \textit{global} phase and thus irrelevant to the probabilities. In any other case, the difference between $\theta_1$ and $\theta_2$ enforces $|\psi\rangle$ and $|\phi\rangle$ to be distinct. This difference that matters is known as the \textit{relative phase}. 

\section{Continuous symmetries and generating translations} %%%%%%%%%%%%%%%%%%%

A common idea in the development of quantum mechanics has been to take a piece of classical theory and then come up with a way to quantize it. Such is also the approach of \textit{first quantization}: we replace all momenta and coordinate variables by the operators $\hat{p}$ and $\hat{x}$, then demand $[\hat{x},\hat{p}]=i\hbar$ to quantize. As $\hbar$ determines the relevant scale, the rule of thumb is that it should be present in quantum mechanical equations. Next, notice that when a particle has non-zero momentum in some frame of reference, its coordinates must be changing with time. Hence we say that momentum is the \textit{generator} of spatial translations. To construct the operator $\hat{\mathscr{J}}$ for inifinitesimal translations, we demand that it has the following properties:
\begin{align*}
(\textrm{i})  &~\hat{\mathscr{J}}(\delta x)|x\rangle = |x+\delta x\rangle
               \implies \hat{\mathscr{J}}(\delta x\rightarrow0)=\hat{I},\\
(\textrm{ii}) &~\hat{\mathscr{J}}(\delta x_1) \hat{\mathscr{J}}(\delta x_2)
               = \hat{\mathscr{J}}(\delta x_1+\delta x_2),\\
(\textrm{iii})&~\hat{\mathscr{J}}^{-1}(\delta x)
               = \hat{\mathscr{J}}(-\delta x),\\
(\textrm{iv}) &~\hat{\mathscr{J}}^\dagger(\delta x)
                \hat{\mathscr{J}}(\delta x) = \hat{I}.
\end{align*}
To first order in the infinitesimal $\delta x$, the translation generated by $\hat{p}$ should then be of the form
\begin{equation}
\hat{\mathscr{J}}(\delta x) = \hat{I} + C\delta x \hat{p}.
\label{translation}
\end{equation}
The above properties and the requirement that both terms on the left-hand side of Eq.~\eqref{translation} be dimensionless let us determine the constant $C$. The result is
\begin{equation}
\hat{\mathscr{J}}(\delta x) = \hat{I} - \frac{i}{\hbar}\delta x \hat{p}.
\label{translation}
\end{equation}
By virtue of property $(\textrm{ii})$, a finite translation of length $\Delta x$ can then be obtained via $n \rightarrow \infty$ successive infinitesimal translations of length $\delta x \equiv \Delta x/n$. This leads to
\begin{equation*}
\mathscr{J}(\Delta x)
=
\lim_{n \rightarrow \infty}
(\mathscr{J}(\delta x))^n
=
\lim_{n \rightarrow \infty}
\left(\hat{I}-\frac{i}{\hbar}\hat{p} \delta x\right)^n
=
\lim_{n \rightarrow \infty}
\left(\hat{I}-\frac{i\hat{p}_x \Delta x}{\hbar n}\right)^n
=
e^{-i \Delta x\hat{p}_x/\hbar}.
\end{equation*}
\textit{Time-evolution} then again is generated by the Hamiltonian, the Hermitian operator which in quantum mechanics is practically always associated with the system's total energy. We therefore define the infinitesimal operator advancing the system's time by $\delta t$ as
\begin{equation}
\hat{U}(\delta t) = \hat{I} - \frac{i}{\hbar}\delta t\hat{H}.
\label{time-evolution}
\end{equation}
A calculation identical to that of finite translations will now yield nothing but the familiar time-evolution operator
\begin{equation*}
\hat{U}(\Delta t) = e^{-i\Delta t\hat{H}/\hbar}.
\end{equation*}
The usual notation for time-evolution from $t_0$ to $t_1 = t_0 + \Delta t$ is
\begin{equation*}
|\psi(t + \Delta t)\rangle
=
\hat{U}(t + \Delta t,t)|\psi(t)\rangle
\end{equation*}
or, if we set $t_0 = 0$, the common shorthand is 
$|\psi(\Delta t)\rangle
=
\hat{U}(\Delta t)|\psi(t)\rangle
=
e^{-i\Delta t\hat{H}/\hbar}|\psi(0)\rangle$.

At this point, the reader is encouraged to show a couple of features for this kind of time-evolution:
\begin{itemize}
\item Expectation values are constant in time, i.e.
$\langle \hat{A} \rangle_{t = 0} = \langle \hat{A} \rangle_{t\neq 0} \equiv \langle\psi(t)|\hat{A}|\psi(t)\rangle$.

\item Eigenstates of a time-independent $\hat{H}$ are constant in time (ergo their time derivative vanishes). They are hence called \textit{stationary states}.
\end{itemize}
There is nonetheless an important caveat to be made: \textit{we have assumed that $\hat{H}$ is time-independent}. This wouldn't be the case if we e.g. allow for interactions -- this should be covered on later courses.

\textbf{Nice-to-know: more on continuous symmetries}

Eq.s~\eqref{translation} and~\eqref{time-evolution} are of a certain form involving the generators of the translation in question. In fact, they come from a general form for infinitesimal changes caused by some unitary symmetry operator of the form
\begin{equation*}
\hat{s}(\epsilon) = \hat{I} - \frac{i\epsilon}{\hbar}\hat{G},
\end{equation*}
where $\epsilon$ is an infinitesimal number and $\hat{G}$ is the Hermitian generator of the symmetry in question. We refer to symmetries because they are related to conservation laws: for example, a system possessing rotation symmetry (i.e. is invariant under rotations) conserves angular momentum (which is the generator of rotations). The result that \textit{to each symmetry there is a corresponding conserved quantity} is the essential content of \textbf{Noether's theorem}.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% BIBLIOGRAPHY %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\printbibliography[heading=bibliography]

\end{document}

