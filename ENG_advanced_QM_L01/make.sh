#Document name:
DOC="AQM_lecture_01"
#Remove old version:
rm $DOC.pdf
#Compile:
pdflatex $DOC.tex
biber $DOC
pdflatex $DOC.tex
#mf *.mf			#Call metafont for Feynman diagrams
pdflatex $DOC.tex
#pdflatex $DOC.tex
#Remove auxiliary files:
rm *.aux *.log *.toc
#rm *.600pk *2602gf *.tfm *.mf
rm *.bcf *.out *.run.xml *.blg *.bbl

